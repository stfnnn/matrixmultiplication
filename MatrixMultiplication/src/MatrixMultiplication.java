public class MatrixMultiplication {

	public static void main(String[] args) {

        int[][] prvaMatrica = {{5, -1, 4}, {2, 3, 4}, {4, 9, 4}};
        int[][] drugaMatrica = {{5, -7}, {6, 2}, {3, -9}};
 
        
        try
        { 
        	 int[][] rezultat = mnozenje(prvaMatrica, drugaMatrica);
        	 for(int[] red:rezultat) {
                 for (int kolona:red) {
                     System.out.print(kolona + "    ");
                 }
                 System.out.println();
             }
     	
        } 
        
        catch(Exception ex) 
        { 
           System.out.println("Matrice se ne mogu pomnoziti!");  
        } 
        
	}
		

	public static int[][] mnozenje(int[][] prvaMatrica, int[][] drugaMatrica) {
		
        int[][] rezultat = new int[prvaMatrica.length][drugaMatrica[0].length];
        for(int i = 0; i<prvaMatrica.length; i++) {
            for (int j = 0; j<drugaMatrica[0].length; j++) {
                for (int k = 0; k<prvaMatrica[0].length; k++) {
                	rezultat[i][j] += prvaMatrica[i][k]*drugaMatrica[k][j];
                               
                }
            }
        }

        return rezultat;
    }
}
